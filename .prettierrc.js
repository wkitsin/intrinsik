module.exports = {
  bracketSpacing: false,
  jsxBracketSameLine: true,
  singleQuote: false,
  trailingComma: 'none',
  printWidth: 80,
  bracketSpacing: true,
  arrowParens: 'avoid',
  jsxBracketSameLine: false,
};
