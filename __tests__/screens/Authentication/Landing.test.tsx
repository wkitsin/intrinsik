/**
 * @format
 */

import React from "react";

import renderer from "react-test-renderer";
import { NavigationContainer } from "@react-navigation/native";
import Landing from "../../../src/screens/Authentication/Landing/Landing";
import { render, fireEvent } from "@testing-library/react-native";
import jestProps from "../../../src/helpers/JestProps";

jest.useFakeTimers();

const LandingTest = () => (
  <NavigationContainer>
    <Landing {...jestProps} />
  </NavigationContainer>
);

it("renders correctly", async () => {
  const tree = renderer.create(<LandingTest />);
  expect(tree).toMatchSnapshot();

  const { getByText } = render(<LandingTest />);
  fireEvent.press(getByText("Sign In"));
  expect(jestProps.navigation.navigate).toHaveBeenCalledWith("SignIn");
});
