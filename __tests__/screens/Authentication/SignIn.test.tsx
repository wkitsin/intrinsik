/**
 * @format
 */

import React from "react";
import SignIn from "../../../src/screens/Authentication/SignIn/SignIn";

import renderer from "react-test-renderer";
import { StoreProvider } from "easy-peasy";
import store from "../../../src/store/actions";
import { NavigationContainer } from "@react-navigation/native";

jest.useFakeTimers();

const SignInTest = () => (
  <StoreProvider store={store}>
    <NavigationContainer>
      <SignIn />
    </NavigationContainer>
  </StoreProvider>
);

it("renders correctly", () => {
  const tree = renderer.create(<SignInTest />);
  expect(tree).toMatchSnapshot();
});
