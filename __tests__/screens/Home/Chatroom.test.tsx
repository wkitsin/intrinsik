/**
 * @format
 */

import React from "react";

import renderer from "react-test-renderer";
import { NavigationContainer } from "@react-navigation/native";
import Chatroom from "../../../src/screens/Home/Chatroom/Chatroom";
import { StoreProvider } from "easy-peasy";
import store from "../../../src/store/actions";

jest.useFakeTimers();

it("renders correctly", () => {
  const tree = renderer.create(
    <StoreProvider store={store}>
      <NavigationContainer>
        <Chatroom />
      </NavigationContainer>
    </StoreProvider>
  );
  expect(tree).toMatchSnapshot();
});
