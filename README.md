Simple Chatfroom for Intrinsik 

```
1. State management library - Easy-Peasy
2. Screen Navigation - React Navigation
3. Data and API services - Fake JSON 
4. App should work on iOS and Android, but it was developed from iOS
```

Limitations
```
1. All Chatroom and Messages are dummy data 
2. There will be 3 chatrooms, but each of them are the same
3. Session is saved in AsyncStorage, however killing the app will reset the chatroom
4. Since there is no database, any valid email and password will log you in
5. Due to time constraint, the test coverage is not complete
```

Installation 
```
 1. yarn 
 2. cd ios && pod install
```
A small clip of the app
https://cln.sh/7gvg8f
