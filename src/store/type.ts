import { Action, createTypedHooks, Thunk } from "easy-peasy";

interface UserSession {
  userEmail: string | undefined;
}

interface Inbox {
  name: string;
  message: string;
  lastUpdated: string;
}

export interface Message {
  _id: string | number;
  text: string;
  createdAt: Date;
  received?: boolean;
  image?: string;
  fileUri?: string;
  type: "image" | "file" | "text";
  user: {
    _id: string | number;
    name?: string;
  };
}

export interface UserStoreModel {
  userSession: UserSession;
  setUserSession: Action<UserStoreModel, UserSession>;
  removeUserSession: Thunk<UserStoreModel>;
  logInSessions: Thunk<UserStoreModel>;
}

export interface InboxStoreModel {
  inbox: Inbox[];
  setInbox: Action<InboxStoreModel, Inbox[]>;
  fetchInbox: Thunk<InboxStoreModel>;
}

export interface ChatroomStoreModel {
  chatroom: Message[];
  setMessage: Action<ChatroomStoreModel, Message>;
  fetchMessages: Thunk<ChatroomStoreModel>;
  setReadMessage: Action<ChatroomStoreModel>;
}

const typedHooks = createTypedHooks<
  UserStoreModel & InboxStoreModel & ChatroomStoreModel
>();

export const useStoreActions = typedHooks.useStoreActions;
export const useStoreDispatch = typedHooks.useStoreDispatch;
export const useStoreState = typedHooks.useStoreState;
