import AsyncStorage from "@react-native-async-storage/async-storage";
import { createStore, action, thunk, persist } from "easy-peasy";
import { ChatroomStoreModel, InboxStoreModel, UserStoreModel } from "./type";
import fetchData from "../api/fetchData";
import update from "immutability-helper";

const store = createStore<
  UserStoreModel & InboxStoreModel & ChatroomStoreModel
>({
  userSession: persist({
    userEmail: undefined
  }),
  setUserSession: action((state, payload) => {
    state.userSession = update(state.userSession, { $set: payload });
  }),
  removeUserSession: thunk(async actions => {
    await AsyncStorage.removeItem("userEmail");
    actions.setUserSession({ userEmail: undefined });
  }),
  logInSessions: thunk(async actions => {
    fetchData({
      method: "POST",
      data: {
        email: "internetEmail",
        username: "nameFirst"
      }
    }).then(async result => {
      await AsyncStorage.setItem("userEmail", result.email);
      actions.setUserSession({ userEmail: result.email });
    });
  }),

  inbox: [],
  setInbox: action((state, payload) => {
    state.inbox = update(state.inbox, { $push: payload });
  }),
  fetchInbox: thunk(actions =>
    fetchData({
      method: "POST",
      data: {
        name: "nameFirst",
        message: "stringWords",
        lastUpdated: "date",
        _repeat: 3
      }
    }).then(results => {
      actions.setInbox(results);
    })
  ),

  chatroom: [],
  setMessage: action((state, payload) => {
    const chatroom = update(state.chatroom, { $push: [payload] }).sort(
      (a, b) => {
        return (
          new Date(b.createdAt).valueOf() - new Date(a.createdAt).valueOf()
        );
      }
    );

    state.chatroom = chatroom;
  }),
  fetchMessages: thunk((actions, payload, helpers) => {
    // assume that the other person has an id of 1
    const { chatroom } = helpers.getState();
    const recipient = chatroom.find(chat => chat.user._id === 1);

    fetchData({
      method: "POST",
      data: {
        _id: "stringDigits",
        text: "stringShort",
        createdAt: new Date(),
        user: {
          _id: recipient ? recipient.user._id : 1,
          name: recipient ? recipient.user.name : "nameFirst"
        }
      }
    }).then(results => {
      actions.setMessage({
        ...results,
        createdAt: new Date()
      });

      actions.setReadMessage();
    });
  }),
  setReadMessage: action(state => {
    const readChatroom = state.chatroom.map(message => {
      if (message.user._id === 2 && !message.received) {
        return { ...message, received: true };
      } else {
        return message;
      }
    });

    state.chatroom = readChatroom;
  })
});

// Hot Reloading Logic
if (process.env.NODE_ENV === "development") {
  if (module.hot) {
    module.hot.accept("./model", () => {
      store.reconfigure(model); // 👈 Here is the magic
    });
  }
}

export default store;
