/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import "react-native-gesture-handler";
import { ThemeProvider } from "react-native-elements";
import React from "react";
import StackNavigation from "./navigation/StackNavigation";
import { StoreProvider } from "easy-peasy";
import store from "./store/actions";

const App: React.FC = () => {
  return (
    <StoreProvider store={store}>
      <ThemeProvider>
        <StackNavigation />
      </ThemeProvider>
    </StoreProvider>
  );
};

export default App;
