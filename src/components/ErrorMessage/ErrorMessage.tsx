import * as React from "react";
import { ErrorMessage as HookFormErrorMessage } from "@hookform/error-message";
import { Text } from "react-native-elements";
import { dimensions } from "../../helpers/Dimensions";
import { DeepMap, FieldError } from "react-hook-form";

interface ErrorMessageProps {
  name: string;
  errors: DeepMap<any, FieldError>;
}

const ErrorMessage: React.FC<ErrorMessageProps> = ({ errors, name }) => (
  <HookFormErrorMessage
    errors={errors}
    name={name}
    render={({ message }) => (
      <Text
        style={{
          color: "red",
          marginLeft: dimensions.padding,
          marginBottom: 20
        }}
      >
        {message}
      </Text>
    )}
  />
);

export default ErrorMessage;
