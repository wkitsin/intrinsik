import * as React from "react";
import { ActivityIndicator } from "react-native";
import Container from "../container/Container";

const LoadingIndicator: React.FC = () => {
  return (
    <Container full centered>
      <ActivityIndicator />
    </Container>
  );
};

export default LoadingIndicator;
