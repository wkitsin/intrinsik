const url = "https://app.fakejson.com/q";
const token = "hym9M1_XSfzy9eVnmPnrMQ";

const fetchData = async ({ data = {}, method = "GET" }) => {
  const response = await fetch(url, {
    method,
    headers: {
      "Content-Type": "application/json"
    },

    body: JSON.stringify({
      token,
      data
    })
  });

  return await response.json();
};

export default fetchData;
