import * as React from "react";
import {
  GiftedChat,
  IMessage,
  MessageText,
  MessageTextProps
} from "react-native-gifted-chat";
import { useStoreActions, useStoreState } from "../../../store/type";
import { HomeStackNavProps } from "../HomeParamsList";
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import { Alert, TouchableOpacity, View } from "react-native";
import { launchImageLibrary } from "react-native-image-picker";
import { Message } from "../../../store/type";
import DocumentPicker from "react-native-document-picker";

const Chatroom: React.FC<HomeStackNavProps<"Chatroom">> = () => {
  const { chatroom } = useStoreState(state => state);
  const { fetchMessages, setMessage } = useStoreActions(actions => actions);

  const onSend = React.useCallback((message: Message) => {
    setMessage({ ...message, received: false });

    // To mimick the receiving messages
    setTimeout(() => {
      fetchMessages();
    }, 500);
  }, []);

  const onPressCamera = () => {
    launchImageLibrary({ mediaType: "photo" }, response => {
      if (response.uri) {
        onSend({
          _id: Math.round(Math.random() * 1000000).toString(),
          text: "Send An Image",
          createdAt: new Date(),
          image: response.uri,
          type: "image",
          user: {
            _id: 2
          }
        });
      } else {
        Alert.alert("We failed to send the photo please try again");
      }
    });
  };

  const onPressFile = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles]
      });

      onSend({
        _id: Math.round(Math.random() * 1000000).toString(),
        text: "Send A File",
        createdAt: new Date(),
        fileUri: res.uri,
        type: "file",
        user: {
          _id: 2
        }
      });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        console.log({ err });
      } else {
        throw err;
      }
    }
  };

  const renderAccessory = () => (
    <View style={{ display: "flex", flexDirection: "row" }}>
      <TouchableOpacity
        style={{ marginHorizontal: 10 }}
        onPress={onPressCamera}
      >
        <MaterialIcon
          name="photo-camera"
          color="blue"
          size={25}
          style={{ marginRight: 10 }}
        />
      </TouchableOpacity>
      <TouchableOpacity onPress={onPressFile}>
        <MaterialIcon
          name="attach-file"
          color="blue"
          size={25}
          style={{ marginRight: 10 }}
        />
      </TouchableOpacity>
    </View>
  );

  const renderMessageText = (props: MessageTextProps<Message>) => {
    const { currentMessage } = props;

    if (currentMessage && currentMessage.fileUri) {
      return (
        <MaterialIcon
          name="attach-file"
          color="white"
          size={30}
          style={{ margin: 10 }}
        />
      );
    }

    return <MessageText {...props} />;
  };

  return (
    <GiftedChat
      user={{ _id: 2 }}
      showUserAvatar={false}
      messages={chatroom}
      onSend={message => onSend({ ...message[0], type: "text" })}
      renderAccessory={renderAccessory}
      renderMessageText={renderMessageText}
      renderTicks={(message: IMessage) => (
        <>
          {message.user._id === 2 && message.received ? (
            <MaterialIcon
              name="done-all"
              color="white"
              style={{ marginRight: 10 }}
            />
          ) : message.user._id === 2 ? (
            <MaterialIcon
              name="done"
              color="white"
              style={{ marginRight: 10 }}
            />
          ) : null}
        </>
      )}
    />
  );
};

export default Chatroom;
