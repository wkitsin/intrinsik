import { StyleSheet } from "react-native";
import { dimensions } from "../../../helpers/Dimensions";

const styles = StyleSheet.create({
  timeAgo: {
    fontSize: 12
  },
  listItem: {
    width: "100%",
    padding: dimensions.padding,
    borderBottomWidth: 1,
    backgroundColor: "white"
  }
});

export default styles;
