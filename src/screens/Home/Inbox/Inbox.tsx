import * as React from "react";
import { FlatList, TouchableWithoutFeedback, View } from "react-native";
import { Button, Divider, Text } from "react-native-elements";
import TimeAgo from "react-native-timeago";
import LoadingIndicator from "../../../components/LoadingIndicator/LoadingIndicator";
import { dimensions } from "../../../helpers/Dimensions";
import { useStoreActions, useStoreState } from "../../../store/type";
import { HomeStackNavProps } from "../HomeParamsList";
import styles from "./Inbox.styles";

const Inbox: React.FC<HomeStackNavProps<"Inbox">> = ({ navigation }) => {
  const { fetchInbox, removeUserSession } = useStoreActions(actions => actions);
  const inbox = useStoreState(state => state.inbox);

  React.useEffect(() => {
    if (inbox.length < 1) {
      fetchInbox();
    }
  }, [inbox]);

  React.useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Button
          type="clear"
          title="Sign Out"
          onPress={async () => {
            removeUserSession();
          }}
        />
      )
    });
  }, []);

  if (inbox.length === 0) {
    return <LoadingIndicator />;
  }

  return (
    <FlatList
      data={inbox}
      keyExtractor={item => item.lastUpdated}
      renderItem={({ item: { name, message, lastUpdated }, index }) => (
        <TouchableWithoutFeedback
          key={index}
          onPress={() => {
            navigation.navigate("Chatroom");
          }}
          style={{
            width: dimensions.screenWidth
          }}
        >
          <View
            style={{
              paddingHorizontal: dimensions.padding
            }}
          >
            <View
              style={{
                marginVertical: 10,
                display: "flex",
                alignItems: "center",
                flexDirection: "row",
                justifyContent: "space-between"
              }}
            >
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <Text style={{ marginRight: 10 }}>{index + 1}. </Text>
                <View>
                  <Text>{name}</Text>
                  <Text>{message}</Text>
                </View>
              </View>

              <TimeAgo time={lastUpdated} style={styles.timeAgo} />
            </View>

            <Divider />
          </View>
        </TouchableWithoutFeedback>
      )}
    />
  );
};

export default Inbox;
