import * as React from "react";
import { Controller, useForm } from "react-hook-form";
import { Button, Input } from "react-native-elements";
import Container from "../../../components/container/Container";
import { AuthenticationStackNavProps } from "../AuthenticationParamsList";
import { useStoreActions } from "../../../store/type";
import { signInSchema } from "../../../helpers/Validation";
import { yupResolver } from "@hookform/resolvers/yup";
import ErrorMessage from "../../../components/ErrorMessage/ErrorMessage";

const SignIn: React.FC<AuthenticationStackNavProps<"SignIn">> = ({
  navigation
}) => {
  const { logInSessions } = useStoreActions(actions => actions);

  const { errors, control, trigger } = useForm({
    defaultValues: { email: "", password: "" },
    resolver: yupResolver(signInSchema)
  });

  React.useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Button
          title="Sign In"
          type="clear"
          style={{ marginHorizontal: 20 }}
          onPress={async () => {
            const result = await trigger();

            if (result) {
              logInSessions();
            }
          }}
        />
      )
    });
  }, []);

  return (
    <Container>
      <Controller
        control={control}
        render={({ onChange }) => (
          <Input
            autoCapitalize="none"
            placeholder="Enter your email"
            onChangeText={value => onChange(value)}
            label="Email"
            labelStyle={{ color: "black" }}
          />
        )}
        name="email"
        rules={{ required: true }}
        defaultValue=""
      />
      {errors.email && <ErrorMessage name="email" errors={errors} />}

      <Controller
        control={control}
        render={({ onChange }) => (
          <Input
            secureTextEntry
            label="Password"
            labelStyle={{ color: "black" }}
            placeholder="Enter your password"
            onChangeText={value => onChange(value)}
          />
        )}
        name="password"
        rules={{ required: true }}
        defaultValue=""
      />
      {errors.password && <ErrorMessage name="password" errors={errors} />}
    </Container>
  );
};

export default SignIn;
