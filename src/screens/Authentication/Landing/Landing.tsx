import * as React from "react";
import { Button, Text } from "react-native-elements";
import Container from "../../../components/container/Container";
import { AuthenticationStackNavProps } from "../AuthenticationParamsList";

const Landing: React.FC<AuthenticationStackNavProps<"Landing">> = ({
  navigation
}) => (
  <Container centered>
    <Text h1 style={{ marginBottom: 40 }}>
      Welcome to the chatting system!
    </Text>
    <Button
      title="Sign In"
      onPress={() => {
        navigation.navigate("SignIn");
      }}
    />
  </Container>
);

export default Landing;
