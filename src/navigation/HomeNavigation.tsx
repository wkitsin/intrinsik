import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { HomeParamsList } from "../screens/Home/HomeParamsList";
import Inbox from "../screens/Home/Inbox/Inbox";
import Chatroom from "../screens/Home/Chatroom/Chatroom";

const Stack = createStackNavigator<HomeParamsList>();

const HomeNavigation = () => (
  <Stack.Navigator screenOptions={{ headerBackTitle: " " }}>
    <Stack.Screen name="Inbox" component={Inbox} />
    <Stack.Screen name="Chatroom" component={Chatroom} />
  </Stack.Navigator>
);

export default HomeNavigation;
