import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import AuthenticationNavigation from "./AuthenticationNavigation";
import HomeNavigation from "./HomeNavigation";
import { useStoreActions, useStoreState } from "../store/type";
import { useStoreRehydrated } from "easy-peasy";
import AsyncStorage from "@react-native-async-storage/async-storage";
import LoadingIndicator from "../components/LoadingIndicator/LoadingIndicator";

const StackNavigation = () => {
  const isRehydrated = useStoreRehydrated();
  const [loading, setLoading] = React.useState(true);
  const { setUserSession } = useStoreActions(actions => actions);

  const userSession = useStoreState(state => state.userSession);

  React.useEffect(() => {
    AsyncStorage.getItem("userEmail").then(result => {
      if (result) {
        setUserSession({ userEmail: result });
      }
    });
  }, []);

  React.useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  }, [userSession]);

  if (loading || !isRehydrated) {
    return <LoadingIndicator />;
  }

  return (
    <NavigationContainer>
      {userSession.userEmail ? HomeNavigation() : AuthenticationNavigation()}
    </NavigationContainer>
  );
};

export default StackNavigation;
