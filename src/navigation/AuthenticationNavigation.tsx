import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { AuthenticationParamsList } from "../screens/Authentication/AuthenticationParamsList";
import Landing from "../screens/Authentication/Landing/Landing";
import SignIn from "../screens/Authentication/SignIn/SignIn";

const Stack = createStackNavigator<AuthenticationParamsList>();

const AuthenticationNavigation = () => (
  <Stack.Navigator screenOptions={{ headerBackTitle: " " }}>
    <Stack.Screen
      name="Landing"
      component={Landing}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="SignIn"
      component={SignIn}
      options={{ headerTitle: " " }}
    />
  </Stack.Navigator>
);

export default AuthenticationNavigation;
