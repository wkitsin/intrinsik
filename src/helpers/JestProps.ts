const createTestProps = (props: Object) => ({
  navigation: {
    navigate: jest.fn()
  },
  ...props
});

const jestProps = createTestProps({});

export default jestProps;
