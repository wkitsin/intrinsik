import * as yup from "yup";

export const signInSchema = yup.object().shape({
  email: yup.string().email().required("Email cannot be blank"),
  password: yup.string().required("Password cannot be blank")
});
