import { Dimensions } from "react-native";

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
const itemWidth = screenWidth / 2;
const gridGap = 10;

export const dimensions = {
  screenWidth: screenWidth,
  screenHeight: screenHeight,
  padding: gridGap
};
